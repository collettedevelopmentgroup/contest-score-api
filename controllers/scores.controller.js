var scoreService = require('../services/score.service');

function success(res, msg, data) {
    var payload = {
        'message': msg || 'Call was successful!'
    };

    if (data) {
        payload['data'] = data;
    }

    res.status(200)
        .json(payload);
}

function error(res, msg) {
    res.status(500)
        .json({
            'message': msg || 'Call could not complete due to error!'
        });
}

/*
Get scores - Should return top 25 contestant. A contestant record consists of a name
and number of pies eaten. Only each contestant's highest score should be reported.
 */

exports.getScores = function (req, res) {
    scoreService.getScores(function (result) {
        success(res, result.msg, result.data);
    })
};

/*
Record score - Should allow a score to be added. Parameters are name of contestant
and number of pies eaten.
 */

exports.postAddScore = function (req, res) {
    scoreService.addScore(req.body, function (result) {
        if (!result.status) {
            error(res, result.msg);
        }

        success(res, result.msg);
    });
};

/*
Delete score - Should allow deletion of a score in case of disqualification.
 */

exports.deleteRemoveScore = function (req, res) {
    scoreService.deleteScore(req.params,function (result) {
        if (!result.status) {
            error(res, result.msg);
        }

        success(res, result.msg);
    });
};