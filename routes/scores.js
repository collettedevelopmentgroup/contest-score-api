var express = require('express');
var router = express.Router();

var scoresController = require('../controllers/scores.controller');

/*
Get scores - Should return top 25 contestant. A contestant record consists of a name
and number of pies eaten. Only each contestant's highest score should be reported.
 */

router.get('/', scoresController.getScores);

/*
Record score - Should allow a score to be added. Parameters are name of contestant
and number of pies eaten.
 */

router.post('/', scoresController.postAddScore);

/*
Delete score - Should allow deletion of a score in case of disqualification.
 */

router.delete('/:contestant_id', scoresController.deleteRemoveScore);

module.exports = router;