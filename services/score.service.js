var Contestant = require('../models/Contestant');
var Firebase = require('../gateways/firebase.gateway');

var scores = Firebase.ref('scores');

exports.getScores = function (callback) {
    scores.orderByKey().once('value', function (snapshot) {
        var listOfScoresMap = {};
        var listOfScores = [];

        snapshot.forEach(function (child) {
            var snap = child.val();
            var targetScore = listOfScoresMap[snap.name];
            if (!targetScore) {
                listOfScoresMap[snap.name] = snap;
                return;
            }

            if (snap.no_pies_eaten > targetScore.no_pies_eaten) {
                targetScore = snap;
            }
        });

        for (var prop in listOfScoresMap) {
            listOfScores.push(listOfScoresMap[prop]);
        }

        listOfScores.sort(function (a,b) {
            return a.no_pies_eaten > b.no_pies_eaten ? -1 : (a.no_pies_eaten < b.no_pies_eaten ? 1 : 0);
        });

        callback({
            'data': listOfScores.slice(0, 25)
        });
    })
};

exports.addScore = function (reqBody, callback) {
    if (!reqBody || !reqBody.name || (!reqBody.no_pies_eaten && reqBody.no_pies_eaten !== 0)) {
        callback({
            'status': false,
            'msg': 'ERR-malformed request json: {"name":"", "no_pies_eaten":0}'
        });
    }

    var contestant = Contestant.create(reqBody);

    var newScore = scores.push();

    contestant.id = newScore.key;

    newScore.set(contestant)
        .then(function (snapshot) {
            callback({
                'status': true
            });
        })
        .catch(function (error) {
            callback({
                'status': false,
                'msg': 'ERR-' + error
            });
        });
};

exports.deleteScore = function (reqParams, callback) {
    if (!reqParams.contestant_id) {
        callback({
            'status':false,
            'msg': 'ERR-malformed URL, DELETE /scores/:contestant_id'
        })
    }

    scores.child(reqParams.contestant_id).remove()
        .then(function (snapshot) {
            callback({
                'status': true
            });
        })
        .catch(function (error) {
            callback({
                'status': false,
                'msg': 'ERR-' + error
            });
        });
};