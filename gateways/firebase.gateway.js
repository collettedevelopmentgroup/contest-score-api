var firebase = require("firebase-admin");

var serviceAccount = require("../serviceAccountKey.json");

firebase.initializeApp({
    credential: firebase.credential.cert(serviceAccount),
    databaseURL: "https://node-contestant-score-api.firebaseio.com"
});

var db = firebase.database();

exports.ref = function (ref) {
    return db.ref(ref);
};