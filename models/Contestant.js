var Contestant = function () {
    this.id = '';
    this.name = '';
    this.no_pies_eaten = 0;
};

exports.create = function (data) {
    var contestant = new Contestant();

    for (var prop in data) {
        if (contestant.hasOwnProperty(prop)) {
            contestant[prop] = data[prop];
        }
    }

    return contestant;
};